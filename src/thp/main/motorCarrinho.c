#define ENA 25
#define DIR 26
#define PUL 27  

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/gptimer.h"
#include "esp_log.h"
#include "motorCarrinho.h"
#include "relayModule.h"
#include "mqtt.h"

int steps_remaining = 0;
int motor_running = 1;
gptimer_handle_t gptimer = NULL;
int direction = 1;
bool IRAM_ATTR timer_isr(gptimer_handle_t timer, const gptimer_alarm_event_data_t *edata, void *user_ctx)
{
    static bool pulse_state = false;

    if (steps_remaining > 0 && motorMooving)
    {
        if (pulse_state)
        {
            gpio_set_level(PUL, 0);
            pulse_state = false;
        }
        else
        {
            gpio_set_level(PUL, 1);
            pulse_state = true;
            steps_remaining--;
        }
    }

    return false;
}

void setup_motor()
{
    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << ENA) | (1ULL << DIR) | (1ULL << PUL),
        .mode = GPIO_MODE_OUTPUT,
        .intr_type = GPIO_INTR_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE,
    };
    gpio_config(&io_conf);

    gpio_set_level(ENA, 0);
}

void setup_timer()
{
    gptimer_config_t timer_config = {
        .clk_src = GPTIMER_CLK_SRC_DEFAULT,
        .direction = GPTIMER_COUNT_UP,
        .resolution_hz = 1000000,
    };
    gptimer_new_timer(&timer_config, &gptimer);

    gptimer_event_callbacks_t cbs = {
        .on_alarm = timer_isr,
    };
    gptimer_register_event_callbacks(gptimer, &cbs, NULL);

    gptimer_alarm_config_t alarm_config = {
        .alarm_count = TIMER_INTERVAL_MS * 1000,
        .reload_count = 0,
        .flags.auto_reload_on_alarm = true,
    };
    gptimer_set_alarm_action(gptimer, &alarm_config);
}

void move(int steps, int dir)
{
    printf("Entrou move!\n");

    gpio_set_level(DIR, dir);
    steps_remaining = steps;
    motor_running = 1;

    setup_timer();
    gptimer_enable(gptimer);

    gptimer_start(gptimer);

    while (steps_remaining > 0 && motorMooving && direction == dir)
    {
        vTaskDelay(pdMS_TO_TICKS(10));
    }
    ESP_ERROR_CHECK(gptimer_stop(gptimer));
    ESP_ERROR_CHECK(gptimer_disable(gptimer));
    ESP_ERROR_CHECK(gptimer_del_timer(gptimer));
}

void motor_task(void *pvParameters)
{
    MotorTaskParams *params = (MotorTaskParams *)pvParameters;
    int steps = params->steps;
    
    direction = 1;

    for (int i = 0; i < 10 && motorMooving && direction == 1; i++)
    {
        move(steps, 1);
    }

    direction = 0;
    for (int i = 0; i < 10 && motorMooving; i++)
    {
        move(steps, 0);
    }
    mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "finalizou");
    closeRelay();
    motorMooving = 0;
    vTaskDelete(NULL);
}