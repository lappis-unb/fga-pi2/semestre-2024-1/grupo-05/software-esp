#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "mqtt.h"
#include "globals.h"
#include "motorCarrinho.h"

void infra()
{
    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << SENSOR_PROXIMIDADE) | (1ULL << SENSOR_PROXIMIDADE_INICIO),
        .mode = GPIO_MODE_INPUT,
        .intr_type = GPIO_INTR_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE,
    };

    gpio_config(&io_conf);

    printf("Entre na função\n");

    while (1)
    {

        int estadoSensor = gpio_get_level(SENSOR_PROXIMIDADE);
        int estadoSensorInicio = gpio_get_level(SENSOR_PROXIMIDADE_INICIO);

        if (direction == 1)
        {
            if (estadoSensor == 0)
            {
                printf("Objeto detectado\n");
            }
            else
            {
                printf("Nenhum objeto detectado\n");
                direction = 0;
            }
        } else {
            if (estadoSensorInicio == 0)
            {
                printf("Objeto detectado\n");
            }
            else
            {
                printf("Nenhum objeto detectado\n");
                paraMotor();
            }
        }

        vTaskDelay(pdMS_TO_TICKS(100));
    }
}
