#include <stdio.h>
#include <inttypes.h>
#include "esp_chip_info.h"
#include "esp_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "wifi.h"
#include "mqtt.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "my_nvs.h"
#include "infraVermelho.h"
#include "relayModule.h"
#include "driver/gpio.h"
#include "globals.h"
#include "motorCarrinho.h"
#include "motorLimpador.h"

void conectadoWifi(void *params)
{
    while (true)
    {
        if (xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY))
        {
            mqtt_start();
        }
    }
}

void enviaSinal(void *params)
{
    while (1)
    {
        if (motorMooving == 0)
        {
            mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "finalizou");
        }
        else
        {
            mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "nao-finalizou");
        }
        
        vTaskDelay((1 * 1000) / portTICK_PERIOD_MS);
    }
}

void app_main(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }

    ESP_ERROR_CHECK(ret);

    conexaoWifiSemaphore = xSemaphoreCreateBinary();
    conexaoMQTTSemaphore = xSemaphoreCreateBinary();

    wifi_start();
    xTaskCreate(&conectadoWifi, "Conexão ao MQTT", 4096, NULL, 1, NULL);
    xTaskCreate(&enviaSinal, "Envia Sinal", 4096, NULL, 1, NULL);
    setupRelay();
    setup_motor();
    setup_motor_limpador();
    xTaskCreate(&infra, "Sensor Fim de Curso", 4096, NULL, 1, NULL);

    // xTaskCreate(&gira_motor, "Gira Motor", 4096, NULL, 1, NULL);

    esp_chip_info_t chip_info;
    uint32_t flash_size;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), %s%s%s%s, ",
           CONFIG_IDF_TARGET,
           chip_info.cores,
           (chip_info.features & CHIP_FEATURE_WIFI_BGN) ? "WiFi/" : "",
           (chip_info.features & CHIP_FEATURE_BT) ? "BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "BLE" : "",
           (chip_info.features & CHIP_FEATURE_IEEE802154) ? ", 802.15.4 (Zigbee/Thread)" : "");

    unsigned major_rev = chip_info.revision / 100;
    unsigned minor_rev = chip_info.revision % 100;
    printf("silicon revision v%d.%d, ", major_rev, minor_rev);
    if (esp_flash_get_size(NULL, &flash_size) != ESP_OK)
    {
        printf("Get flash size failed");
        return;
    }

    printf("%" PRIu32 "MB %s flash\n", flash_size / (uint32_t)(1024 * 1024),
           (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    printf("Minimum free heap size: %" PRIu32 " bytes\n", esp_get_minimum_free_heap_size());
}
