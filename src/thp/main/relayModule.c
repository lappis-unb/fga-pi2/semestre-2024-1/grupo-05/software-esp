#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "globals.h"

   
void setupRelay(){
    gpio_set_level(RELAY, 1);
    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << RELAY),
        .mode = GPIO_MODE_OUTPUT,
        .intr_type = GPIO_INTR_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE,
    };

    gpio_config(&io_conf);
}

void openRelay() {
    printf("Entrou open relay\n");

    gpio_set_level(RELAY, 0);
 }


void closeRelay() {
    printf("Entrou close relay\n");
    gpio_set_level(RELAY, 1);
 }
