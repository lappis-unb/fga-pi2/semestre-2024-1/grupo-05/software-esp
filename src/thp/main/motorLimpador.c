#define ENA_LIMPADOR 18
#define DIR_LIMPADOR 19
#define PUL_LIMPADOR 21

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/gptimer.h"
#include "esp_log.h"
#include "motorLimpador.h"
#include "relayModule.h"
#include "mqtt.h"
#include "motorCarrinho.h"

int steps_remaining_limpador = 0;
int motor_running_limpador = 1;
gptimer_handle_t gptimerLimpador = NULL;

bool IRAM_ATTR timer_isr_limpador(gptimer_handle_t timer, const gptimer_alarm_event_data_t *edata, void *user_ctx)
{
    static bool pulse_state = false;

    if (steps_remaining_limpador > 0 && motorMooving)
    {
        if (pulse_state)
        {
            gpio_set_level(PUL_LIMPADOR, 0);
            pulse_state = false;
        }
        else
        {
            gpio_set_level(PUL_LIMPADOR, 1);
            pulse_state = true;
            steps_remaining_limpador--;
        }
    }

    return false;
}

void setup_motor_limpador()
{
    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << ENA_LIMPADOR) | (1ULL << DIR_LIMPADOR) | (1ULL << PUL_LIMPADOR),
        .mode = GPIO_MODE_OUTPUT,
        .intr_type = GPIO_INTR_DISABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .pull_up_en = GPIO_PULLUP_DISABLE,
    };
    gpio_config(&io_conf);

    gpio_set_level(ENA_LIMPADOR, 0);
}

void setup_timer_limpador()
{
    gptimer_config_t timer_config = {
        .clk_src = GPTIMER_CLK_SRC_DEFAULT,
        .direction = GPTIMER_COUNT_UP,
        .resolution_hz = 1000000,
    };
    gptimer_new_timer(&timer_config, &gptimerLimpador);

    gptimer_event_callbacks_t cbs = {
        .on_alarm = timer_isr_limpador,
    };
    gptimer_register_event_callbacks(gptimerLimpador, &cbs, NULL);

    gptimer_alarm_config_t alarm_config = {
        .alarm_count = TIMER_INTERVAL_LIMPADOR_MS * 1000,
        .reload_count = 0,
        .flags.auto_reload_on_alarm = true,
    };
    gptimer_set_alarm_action(gptimerLimpador, &alarm_config);
}

void move_limpador(int steps, int dir)
{
    printf("Entrou move limpador!\n");

    gpio_set_level(DIR_LIMPADOR, dir);
    steps_remaining_limpador = steps;
    motor_running_limpador = 1;

    setup_timer_limpador();
    gptimer_enable(gptimerLimpador);

    gptimer_start(gptimerLimpador);

    while (steps_remaining_limpador > 0 && motorMooving && direction != dir)
    {
        printf("%d\n", steps_remaining_limpador);
        vTaskDelay(pdMS_TO_TICKS(10));
    }
    ESP_ERROR_CHECK(gptimer_stop(gptimerLimpador));
    ESP_ERROR_CHECK(gptimer_disable(gptimerLimpador));
    ESP_ERROR_CHECK(gptimer_del_timer(gptimerLimpador));
}

void motor_task_limpador(void *pvParameters)
{
    MotorLimpadorTaskParams *params = (MotorLimpadorTaskParams *)pvParameters;
    int steps = params->steps;
    

    for (int i = 0; i < 10 && motorMooving && direction == 1; i++)
    {
        move_limpador(steps, 0);
    }

    for (int i = 0; i < 10 && motorMooving; i++)
    {
        move_limpador(steps, 1);
    }
    vTaskDelete(NULL);
}