#ifndef RELAY_H
#define RELAY_H

void setupRelay();
void openRelay();
void closeRelay();

#endif /* RELAY_H */