#ifndef MOTOR_CARRINHO_H
#define MOTOR_CARRINHO_H

#include "driver/gpio.h"
#include "driver/gptimer.h"

// Intervalo do timer (milissegundos)
#define TIMER_INTERVAL_MS 6.4  

// Variáveis globais
extern int steps_remaining;
extern int motor_running;
extern int direction;
// Prototipagem das funções
bool IRAM_ATTR timer_isr(gptimer_handle_t timer, const gptimer_alarm_event_data_t *edata, void *user_ctx);
void setup_motor();
void setup_timer();
void move(int steps, int dir);
void motor_task(void *params);
typedef struct {
    int steps;
} MotorTaskParams;

#endif // MOTOR_CARRINHO_H
