#ifndef MOTOR_LIMPADOR_H
#define MOTOR_LIMPADOR_H

#include "driver/gpio.h"
#include "driver/gptimer.h"

// Intervalo do timer (milissegundos)
#define TIMER_INTERVAL_LIMPADOR_MS 3.2

// Variáveis globais
extern int steps_remaining;
extern int motor_running;

// Prototipagem das funções
bool IRAM_ATTR timer_isr_limpador(gptimer_handle_t timer, const gptimer_alarm_event_data_t *edata, void *user_ctx);
void setup_motor_limpador();
void setup_timer_limpador();
void move_limpador(int steps, int dir);
void motor_task_limpador(void *params);
typedef struct {
    int steps;
} MotorLimpadorTaskParams;

#endif // MOTOR_LIMPADOR_H
