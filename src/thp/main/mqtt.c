#include "mqtt.h"
#include "cJSON.h"
#include "esp_log.h"
#include "relayModule.h"
#include "motorCarrinho.h"
#include "motorLimpador.h"

static esp_mqtt_client_handle_t client;
int powerSaving = 0;
int motorMooving = 0;

TaskHandle_t motorTaskHandle = NULL;
TaskHandle_t motorLimpadorTaskHandle = NULL;

MotorTaskParams motorParamsFront = {
    .steps = 140,
};

MotorLimpadorTaskParams motorLimpadorParams = {
    .steps = 280,
};

void handle_response(char *data)
{
    cJSON *root = cJSON_Parse(data);

    cJSON *method = cJSON_GetObjectItem(root, "method");
    if (method == NULL)
    {
        ESP_LOGE(TAG_M, "Method not found in JSON");
        cJSON_Delete(root);
        return;
    }

    if (!strcmp(method->valuestring, "lowPower"))
    {
        cJSON *params = cJSON_GetObjectItem(root, "params");
        if (params == NULL)
        {
            ESP_LOGE(TAG_M, "Params not found in JSON");
            cJSON_Delete(root);
            return;
        }

        cJSON *lowPower = cJSON_GetObjectItem(params, "lowPower");
        if (lowPower == NULL)
        {
            ESP_LOGE(TAG_M, "lowPower not found in JSON");
            cJSON_Delete(root);
            return;
        }

        powerSaving = lowPower->valueint;
        printf("PowerSaving = %d\n", powerSaving);
    }

    cJSON_Delete(root);
}

static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
{

    esp_mqtt_client_handle_t client = event->client;
    int msg_id;

    switch (event->event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(TAG_M, "MQTT_EVENT_CONNECTED");

        msg_id = esp_mqtt_client_subscribe(client, "unb/fga/placasolar/FGA-001", 0);
        xSemaphoreGive(conexaoMQTTSemaphore);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG_M, "MQTT_EVENT_DISCONNECTED");
        xSemaphoreTake(conexaoMQTTSemaphore, portMAX_DELAY);
        xSemaphoreGive(conexaoWifiSemaphore);
        break;
    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG_M, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG_M, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG_M, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG_M, "MQTT_EVENT_DATA");
        char topic[100];
        char data[100];
        snprintf(topic, sizeof(topic), "%.*s", event->topic_len, event->topic);
        snprintf(data, sizeof(data), "%.*s", event->data_len, event->data);
        printf("TOPIC=%s\r\n", topic);
        printf("DATA=%s\r\n", data);
        if (strcmp(topic, "unb/fga/placasolar/FGA-001") == 0)
        {
            if (strcmp(data, "dry") == 0)
            {
                motorMooving = 1;
                direction = 1;
                mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "iniciou-limpeza");

                xTaskCreate(motor_task, "motor_task", 2048, &motorParamsFront, 5, &motorTaskHandle);
                xTaskCreate(motor_task_limpador, "motor_task_limpador", 2048, &motorLimpadorParams, 5, &motorLimpadorTaskHandle);

                printf("A SECO\n");
            }
            if (strcmp(data, "wet") == 0)
            {
                motorMooving = 1;
                direction = 1;
                mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "iniciou-limpeza");

                printf("A AGUA\n");
                openRelay();
                xTaskCreate(motor_task, "motor_task", 2048, &motorParamsFront, 5, &motorTaskHandle);
                xTaskCreate(motor_task_limpador, "motor_task_limpador", 2048, &motorLimpadorParams, 5, &motorLimpadorTaskHandle);

            }
            if (strcmp(data, "stop") == 0)
            {
                mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "finalizou");
                printf("STOP\n");
                paraMotor();
            }
            if (strcmp(data, "check") == 0)
            {
                printf("%d\n", motorMooving);
                if(motorMooving == 0) {
                    mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "finalizou");
                } else {
                    mqtt_envia_mensagem("unb/fga/placasolar/FGA-001", "nao-finalizou");
                }
            }
        }
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG_M, "MQTT_EVENT_ERROR");
        break;
    default:
        ESP_LOGI(TAG_M, "Other event id:%d", event->event_id);
        break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG_M, "Event dispatched from event loop base=%s, event_id=%d", base, (int)event_id);
    mqtt_event_handler_cb(event_data);
}

void mqtt_start()
{
    // while (xSemaphoreTake(conexaoMQTTSemaphore, 2) == pdFALSE);

    esp_mqtt_client_config_t mqtt_config = {
        .broker.address.uri = "mqtt://test.mosquitto.org",
    };

    client = esp_mqtt_client_init(&mqtt_config);
    esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
    esp_mqtt_client_start(client);

    // xSemaphoreGive(conexaoMQTTSemaphore);
}

void mqtt_envia_mensagem(char *topico, char *mensagem)
{
    int message_id = -1;
    if (xSemaphoreTake(conexaoMQTTSemaphore, portMAX_DELAY))
    {
        message_id = esp_mqtt_client_publish(client, topico, mensagem, 0, 1, 0);
        xSemaphoreGive(conexaoMQTTSemaphore);
    }

    if (message_id == -1)
    {
        ESP_LOGE(TAG_M, "Topico: %s\nFalha ao enviar a mensagem: %s\n", topico, mensagem);
    }
    else
    {
        ESP_LOGI(TAG_M, "ID da mensagem: %d\nMensagem enviada: %s", message_id, mensagem);
    }
}


void mqtt_disconnect()
{
    while (full_power_mode && !xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY))
        ;

    esp_mqtt_client_stop(client);
    esp_mqtt_client_unregister_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler);
    esp_mqtt_client_disconnect(client);
    esp_mqtt_client_destroy(client);
}

void mqtt_reconnect()
{
    esp_mqtt_client_reconnect(client);
}

void paraMotor()
{
    if (motorTaskHandle != NULL && motorMooving)
    {
        motorMooving = 0;
        motorTaskHandle = NULL;
        motorLimpadorTaskHandle = NULL;
        closeRelay();
    }
}